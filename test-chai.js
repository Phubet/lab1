const chai = require('chai');
const expect = chai.expect;
const math = require('./math');
describe('Test chai', () => {
    it('should compare thing by expect', () => {
        expect(1).to.equals(1);
    });

    it('should compare another things by expect',()=>{
        expect(5>8).to.be.false;
        expect({name:'benz'}).to.deep.equals({name:'benz'});
        expect({name:'benz'}).to.have.property('name').to.equals('benz');
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('benz').to.be.a('string');
        expect('benz'.length).to.equals(4);
        expect('benz').to.length(4);
        expect([1,2,3]).to.length(3);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;
    });
});

describe('Math module',()=>{
    context('Function add1',()=>{
        it('ควรส่งค่ากลับเป็นตัวเลข',()=>{
            expect(math.add1(0,0)).to.be.a('number');
        });
        it('add(1,1) ควรส่งค่ากลับเป็น 2',()=>{
            expect(math.add1(1,1)).to.equals(2);        });
    });
});